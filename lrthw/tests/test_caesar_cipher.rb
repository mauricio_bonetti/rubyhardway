require "lrthw/caesar_cipher.rb"
require "test/unit"

class TestCaesarCipher < Test::Unit::TestCase
  def test_cipher
    assert_equal("a", cipher("a", 0))
    assert_equal("c", cipher("a", 2))
    assert_equal("f", cipher("c", 3))
    assert_equal(",", cipher(",", 100))
    assert_equal("e", cipher("z", 5))  
    assert_equal("dd", cipher("aa", 3))
    assert_equal("okffng-Qwvb", cipher("middle-Outz", 2))
    assert_equal("Lipps_Asvph!", cipher("Hello_World!", 4)) 
  end
end

