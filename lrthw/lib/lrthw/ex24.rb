puts "Let's play with functions that returns many values"

def next_three(num)
  return num + 1, num + 2, num + 3
end

print "Write a number to get a sequence of three: "
num = STDIN.gets.chomp.to_i
puts "Sequence is #{next_three(num)}"

num1, num2, num3 = next_three(num)
puts "First number is #{num1}"
puts "Second number is #{num2}"
puts "Third number is #{num3}"

