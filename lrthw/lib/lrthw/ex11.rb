puts "We're about to start getting input"
print "What is your name? "
name = gets.chomp # gets.chomp -> read the user input without carriage return
puts "So, your name is #{name}"
print "Now, tell me your surname? "
surname = gets # gets -> read the user input including carriage return
print "Now, I got your surname using gets function, see #{surname}"
print "New line!!!\n"
