# Solution for challenge https://www.hackerrank.com/challenges/caesar-cipher-1

# Cipher a String value adding offset to it
def cipher(value, offset)
  raise ArgumentError, "Invalid arguments" if value.empty? || offset < 0 
  split = value.chars
  (0...split.size).each do |i| 
    if split[i] =~ /[A-Za-z]/   
      (0...offset).each do 
        split[i] = split[i].next.chars[0] 
      end
    end
  end
  return split.join
end

def main
  length = Integer(STDIN.gets.chomp)
  string = STDIN.gets.chomp
  offset = Integer(STDIN.gets.chomp)
  puts cipher(string, offset)
end

#main

