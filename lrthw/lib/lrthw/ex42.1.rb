# Classes Review

class Dog
  def initialize(name)
    @name = name
  end

  def to_s
    return "#{@name}"
  end

  attr_accessor :name
end

class Poodle < Dog
  def initialize(name, parent)
    super(name)
    @parent = parent
  end

  def to_s
    return "#{@name} is son of #{@parent}"
  end

  attr_accessor :name, :parent
end

dog1 = Dog.new("Dolly")
dog = Poodle.new("Pampam", dog1)

puts dog
