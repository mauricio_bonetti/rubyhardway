puts "Now we are going to read a file!"
file1 = open(ARGV[0])
puts "Here is the content of the file:"
puts file1.read

puts "Now it is your turn. Write a filename:"
filename = STDIN.gets.chomp
puts "Your filename is #{filename}"
file1 = open(filename)
puts "Here is its content:"
puts file1.read

puts "Now, write a filename that has a number inside:"
filename = STDIN.gets.chomp
file = open(filename)
number = file.read.chomp.to_i
puts "The double of the number is #{number * 2}"

