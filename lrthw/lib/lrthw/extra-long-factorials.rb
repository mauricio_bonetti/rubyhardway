# Solution for - https://www.hackerrank.com/challenges/extra-long-factorials
def factorial(num)
  raise ArgumentError, "Parameter is negative" if num < 0
  return 1 if num <= 1
  fact = 1
  (1..num).each do |i|
    fact *= i
  end  
  fact
end

num = Integer(STDIN.gets.chomp)
puts "#{factorial(num)}"

