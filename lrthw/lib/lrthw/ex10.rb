# Quotes play:
puts "Quotes play"
puts "That's a single quoted string"
puts 'That\'s another single quoted string'
puts 'Here is a double quote:"'
puts "Here is another double quote:\""
# Tabs play:
puts "Tabs play"
puts "The next string is after a tab\tnext-string"
# Slashes play:
puts "Slashes play"
puts "The next string is after a slash \\next-string"
# Next line play:
puts "Next line play"
puts "The next string is in another line\nnext-string"
puts "The next string got a carriage return\rnext-string"

