puts "Lets practice some control structures"
numbers = [1, 2, 3, 4, 5]
puts "Using classic for command:"
for number in numbers
  puts "Number = #{number}"
end

puts "Using a more ruby styled command:"
numbers.each do |number|
  puts "Number = #{number}"
end


mixed = [1, "Mauricio", 2, "Marcelo", 3, "Luciana"]
puts "Using a mixed types array:"
puts "Using a more ruby styled command:"
mixed.each do |element|
   puts "Discovered = #{element}"
end

puts "Using another ruby style way:"
mixed.each { |element| puts "Discovered = #{element}" }


puts "Now we will build a sequence of integers"
sequence = []
(0..10).each do |num|
  puts "Item = #{num}"
end

puts "Now we will build a sequence of strings"
sequence = []
("aa".."cc").each do |num|
  puts "Num = #{num}"
end

