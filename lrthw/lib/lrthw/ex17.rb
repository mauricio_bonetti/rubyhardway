puts "Today we are going to play with files again"
file_from, file_to = ARGV

file_tmp = open(file_from)
content = file_tmp.read

puts "#{file_from} has #{content.length} bytes"
puts "Does the output file(#{file_to}) exist? #{File.exist?(file_to)}"
puts "Ready press RETURN, otherwise CTRL+C"
STDIN.gets

file_to2 = open(file_to, "w")
file_to2.write(content)
file_to2.close
file_tmp.close

puts "Result of file copy:"
file_to2 = open(file_to)
puts file_to2.read
file_to2.close

