puts "Now we are going to play with elsif"

cars = ARGV[0].to_i
people = ARGV[1].to_i

if cars > people
  puts "There are more cars than people"
elsif cars < people
  puts "There are less cars than people"
else
  puts "We have the same number of people and cars"
end

