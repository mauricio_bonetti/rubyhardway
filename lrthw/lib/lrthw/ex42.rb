# Today I'm going to play with classes

class Person
  def initialize(name)
    @name = name
  end

  attr_accessor :name
end

class Employee < Person
  def initialize(name, salary)
    super(name)
    @salary = salary
  end

  def action
    puts "Work!!!"  
  end

  attr_accessor :salary
end

class Gamer < Person
  def initialize(name, game)
    super(name)
    @game = game
  end

  def action
    puts "Play!!!"
  end

  attr_accessor :game
end

people = [Employee.new("Mauricio", "1000"), Gamer.new("Zanardi", "PC")]

people.each do |person|
  person.action
end
