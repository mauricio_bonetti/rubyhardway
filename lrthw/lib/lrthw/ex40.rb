# Now we are going to play with classes!

class Car
  def initialize(model)
    @model = model  
  end

  attr_reader :model

  def print_model()
    puts "Model=#{@model}"
  end
end

car = Car.new("Fiesta")
car.print_model
puts "#{car.model}"
