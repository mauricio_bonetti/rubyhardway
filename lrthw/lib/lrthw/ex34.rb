puts "Today we are going to play with some arrays!!!"

dogs = ["PamPam", "Dolly", "Dany", "Faisca", "Bia", "Pitty"]

dogs.each { |dog| puts "Dog = #{dog}" }

puts "The first dog is #{dogs.first}"
puts "The last dog is #{dogs.pop}"

puts "Let's add another dog to the sequence..."
dogs.push("LaLa") # Adds a dog at the end of array

puts "Let's add another dog to the sequence..."
dogs.unshift("Pepepe", "Dog3") # Adds at first position

puts "Let's add two other dogs..."
dogs.insert(2, "Dog1", "Dog2") # Adds at position 2

puts "Here is the final sequence"
dogs.each { |dog| puts "Dog = #{dog}" }

