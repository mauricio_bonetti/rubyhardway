module Ex25

def Ex25.break_words(sentence)
  words = sentence.split(" ")
  return words
end

def Ex25.print_first_word(words)
   print words.shift
end

def Ex25.print_last_word(words)
  print words.pop
end

def Ex25.sort_words(words)
  return words.sort
end

def Ex25.sort_sentence(sentence)
  words = Ex25.break_words(sentence)
  return Ex25.sort_words(words)
end

end

