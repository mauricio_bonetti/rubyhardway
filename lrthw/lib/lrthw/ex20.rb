puts "Now we are going to work with files using functions"

filename = ARGV[0]

def read_all(filename)
  file = open(filename)
  puts file.read
  file.close
end

def read_line(filename, line_number)
  puts IO.readlines(filename)[line_number - 1]
end

puts "Let's read #{filename} entirely:"
read_all(filename)
print "Write a line to read:"
line_number = STDIN.gets.chomp.to_i
puts "Content of line #{line_number}:"
read_line(filename, line_number)

