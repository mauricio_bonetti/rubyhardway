# Solution for https://www.hackerrank.com/challenges/cavity-map

# Returns a map of Integers
def load_map(map_size)
  map = Array.new(map_size) { Array.new(map_size) }
  (0...map_size).each do |row|
    map[row] = STDIN.gets.chomp.split(//).map { |num| Integer(num) }
  end
  return map
end

def is_deeper?(map, i, j)
  depth = map[i][j]
  n1 = map[i-1][j] == "X" ? 10 : map[i-1][j]
  n2 = map[i+1][j] == "X" ? 10 : map[i+1][j]
  n3 = map[i][j-1] == "X" ? 10 : map[i][j-1]
  n4 = map[i][j+1] == "X" ? 10 : map[i][j+1]
  return depth > n1 && depth > n2 && depth > n3 && depth > n4
end

def fill_cavities(map, map_size)
  (1...map_size - 1).each do |i|
    (1...map_size - 1).each do |j|
      map[i][j] = "X" if is_deeper?(map, i, j)      
    end
  end
end

def print_map(map)
  map.each do |sub_array|
    sub_array.each do |v|
      print "#{v}"
    end
    puts
  end
end

def main
  map_size = Integer(STDIN.gets.chomp)
  raise ArgumentError, "Invalid input" if map_size < 1 || map_size > 100
  map = load_map(map_size)
  fill_cavities(map, map_size)
  print_map(map)
end

main
