# Solution for - https://www.hackerrank.com/challenges/minimum-draws
def find_draws_qty(pairs_of_socks)
  if pairs_of_socks == 1
    return 2
  end
  return pairs_of_socks + 1
end

tests = STDIN.gets.chomp.to_i
test = 1
results = []
while test <= tests
  pairs_of_socks = STDIN.gets.chomp.to_i
  results.push(find_draws_qty(pairs_of_socks))
  test += 1
end
results.each { |result| puts "#{result}" }

