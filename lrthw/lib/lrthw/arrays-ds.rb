# Solution for - https://www.hackerrank.com/challenges/arrays-ds
qty = Integer(STDIN.gets.chomp)
array = Array(STDIN.gets.chomp.split(" "))
(0...qty).each do
  print "#{array.pop} "
end

