puts "Now let's call some functions returning some values"

def add(number_1, number_2)
  puts "Adding #{number_1} and #{number_2}"
  return number_1 + number_2
end

def subtract(number_1, number_2)
  puts "Subtracting #{number_2} from #{number_1}"
  return number_1 - number_2
end

def multiply(number_1, number_2)
  puts "Multiplying #{number_1} by #{number_2} times"
  return number_1 * number_2
end

def divide(numerator, divisor)
  puts "Dividing #{numerator} by #{divisor}"
  return numerator / divisor
end

print "Write a number for arithmetics: "
number_1 = Float(STDIN.gets.chomp)
print "Write another: "
number_2 = Float(STDIN.gets.chomp)

puts add(number_1, number_2)
puts subtract(number_1, number_2)
puts multiply(number_1, number_2)
puts divide(number_1, number_2)

