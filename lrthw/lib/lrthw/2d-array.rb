# Solution for - https://www.hackerrank.com/challenges/2d-array
line1 = STDIN.gets.chomp.split(" ")
line2 = STDIN.gets.chomp.split(" ")
line3 = STDIN.gets.chomp.split(" ")
line4 = STDIN.gets.chomp.split(" ")
line5 = STDIN.gets.chomp.split(" ")
line6 = STDIN.gets.chomp.split(" ")
grid = [line1, line2, line3, line4, line5, line6]

sums = []
(0..3).each do |i|
  (0..3).each do |j|
    sums.push(Integer(grid[i][j]) + Integer(grid[i][j+1]) + Integer(grid[i][j+2]) + Integer(grid[i+1][j+1]) + Integer(grid[i+2][j]) + Integer(grid[i+2][j+1]) + Integer(grid[i+2][j+2]))
  end
end

highest = -100
sums.each do |sum|
  if sum > highest
    highest = sum
  end
end
puts "#{highest}"

