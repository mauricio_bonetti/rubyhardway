def fib(num)
  if num <= 0
    return 0
  elsif num == 1
    return 1
  else
    return fib(num - 1) + fib(num - 2)
  end
end

puts "We are going to evaluate fibonnacci function"
print "Write an integer number: "
num = STDIN.gets.chomp.to_i

puts "fib(#{num}) = #{fib(num)}"
