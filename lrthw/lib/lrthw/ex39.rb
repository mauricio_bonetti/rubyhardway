# Today we are going to play with hashes!!!

states = {
  "São Paulo" => "SP",
  "Paraná" => "PR",
  "Minas Gerais" => "MG",
  "Santa Catarina" => "SC"
}

main_cities = {
  "SP" => "São Paulo",
  "PR" => "Curitiba",
  "MG" => "Belo Horizonte"
}

main_cities["SC"] = "Florianópolis"

# Print main_cities
puts "*" * 10
puts main_cities[states["São Paulo"]]
puts main_cities[states["Paraná"]]
puts main_cities[states["Minas Gerais"]]
puts "*" * 10

