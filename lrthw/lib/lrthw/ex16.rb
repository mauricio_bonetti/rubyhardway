puts "Today we are going to write a file!!!"
filename = ARGV[0]
puts "filename: #{filename}"
file1 = open(filename, "w");
file1.write("It is my first ruby file!!!")
file1.close

puts "Here is the result of your file writing:"
file1 = open(file1)
puts "File content: #{file1.read}"
file1.close

puts "Now we are going to truncate the file"
file1 = open(file1, "w")
file1.truncate(0)
file1.close
puts "File #{filename} was truncated"

puts "Now lets try to read a truncated file"
file1 = open(filename)
puts "File content: #{file1.read}"
file1.close

puts "And last, you are going to write to the file"
print "Write the first line: "
line1 = STDIN.gets.chomp
print "Write the second line: "
line2 = STDIN.gets.chomp
file1 = open(filename, "w")
file1.write(line1)
file1.write("\n")
file1.write(line2)
file1.close

puts "You wrote the file #{filename}, check the result opening the file!"

