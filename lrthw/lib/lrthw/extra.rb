require 'set'

puts "Now we are going to play with Set class"

def put_in_set(*args)
  return args.to_set
end

def to_string(set) 
  return set.inspect
end

set = put_in_set(10, 10, 20, 30, 40, 50, 60, 70)
puts "set = #{to_string(set)}"

