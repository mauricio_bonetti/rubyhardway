# Solution for - https://www.hackerrank.com/challenges/find-point
tests_qty = STDIN.gets.chomp.to_i
if tests_qty < 1 || tests_qty > 15
  return
end
test = 1
simmetrics = []
while test <= tests_qty
  coordinates = STDIN.gets.chomp.split(" ")
  # validate points
  coordinates.each do |coordinate|
    if coordinate.to_i < -100 || coordinate.to_i > 100
      return   
    end
  end 
  point_p = [coordinates[0], coordinates[1]]
  point_q = [coordinates[2], coordinates[3]]
  simmetric = [2 * point_q[0].to_i - point_p[0].to_i, 2 * point_q[1].to_i - point_p[1].to_i]
  simmetrics.push(simmetric)
  test += 1
end
simmetrics.each { |point| puts "#{point[0]} #{point[1]}" }

