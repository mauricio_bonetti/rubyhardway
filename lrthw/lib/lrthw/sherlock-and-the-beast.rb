# Solution for - https://www.hackerrank.com/challenges/sherlock-and-the-beast

def get_decent(num)
  (1..num).reverse_each do |aux|
    return "5" * aux + "3" * (num - aux) if (aux % 3 == 0) && ((num - aux) % 5 ==0)  
  end
  return "3" * num if num % 5 == 0  
  return -1  
end

tests = Integer(STDIN.gets.chomp)
raise ArgumentError, "Invalid tests quantity" if !tests.between?(1, 20)

decent_numbers = []
(1..tests).each do 
  num = Integer(STDIN.gets.chomp)
  raise ArgumentError, "Invalid number" if !num.between?(1, 100000)
  decent_numbers.push(get_decent(num))
end

decent_numbers.each do |number|
  puts "#{number}"
end

