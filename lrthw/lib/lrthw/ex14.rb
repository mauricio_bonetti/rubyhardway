puts "This exercise more about readig inputs and parameters"
username = ARGV[0]
prompt = ">"

puts "Hello #{username}"
puts "I'd like to ask you some questions"
puts "Do you like me #{username}?"
print prompt
answer1 = STDIN.gets.chomp # When using ARGV you cannot use gets alone

puts "Where do you live #{username}?"
print prompt
answer2 = STDIN.gets.chomp

puts "What kind of a computer do you have #{username}?"
print prompt
answer3 = STDIN.gets.chomp

puts """
Alright, #{username}.
Here is what you said when I asked if you like me: #{answer1}
You live in: #{answer2}
And your computer is a: #{answer3}
"""
