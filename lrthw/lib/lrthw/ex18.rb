puts "Now we are going to play with functions!!!"

### Var args
def printNames(*names)
	puts "Names: #{names}"
end
### Two parameter function
def printTwoNames(name1, name2)
	puts "Name1: #{name1}; Name2: #{name2}"
end
### Math function
def doubleValue(number)
	puts "#{number * 2}"	
end

puts "Now I'll call a function:"
printNames("Mauricio", "Eduardo", "Daniel", "Gui", "Giulia")

puts "Now I'll print just two names"
printTwoNames("Mauricio", "Eduardo")

puts "Now I'll try to print more names using two names function"
printTwoNames("Mauricio", "Eduardo")

print "Let's double a number value using functions. Write a number:"
number = STDIN.gets.to_i
puts "double of #{number} is #{doubleValue(number)}"


