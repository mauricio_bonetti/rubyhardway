# Now I'll create a class for songs

class Playlist
  def initialize(songs)
    @songs = songs
  end

  def play_songs()
    @songs.each { |song| puts "#{song}" }
  end
end

list = Playlist.new(["Gimme some love", "I'm a man", "Hammer"])
list.play_songs
