puts "Today we are going to play with while loops!!!"
i = 0
numbers = []
while i < 6 
  numbers.push(i)
  i += 1
end
numbers.each { |num| puts "Num = #{num}" }
puts "Trying another loop..."
numbers.each do |num|
  puts "Num = #{num}"
end
