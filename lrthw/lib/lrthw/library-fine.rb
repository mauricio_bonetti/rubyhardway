# Solution for - https://www.hackerrank.com/challenges/library-fine
require "date"

def in_the_same_year?(date_1, date_2)
  if date_1.year == date_2.year
    return true
  end
  return get_days_between(date_1, date_2) <= 365
end

def in_the_same_month?(date_1, date_2)
  if date_1.month == date_2.month && date_1.year == date_2.year
    return true
  end
  return get_days_between(date_1, date_2) <= 30
end

def get_days_between(date_1, date_2)
  return Integer(date_1 - date_2).abs
end

def get_months_between(date_1, date_2)
  return get_days_between(date_1, date_2) % 30
end

def evaluate_fine(delivered_date, expected_date)
  if expected_date >= delivered_date
    return 0
  end
  if in_the_same_year?(delivered_date, expected_date)
    if in_the_same_month?(delivered_date, expected_date)
      return get_days_between(delivered_date, expected_date) * 15
    else
      return get_months_between(delivered_date, expected_date) * 500
    end
  else
    return 10000
  end
end

d = STDIN.gets.chomp.split(" ")
delivered_date = Date.new(Integer(d[2]), Integer(d[1]), Integer(d[0]))
d = STDIN.gets.chomp.split(" ")
expected_date = Date.new(Integer(d[2]), Integer(d[1]), Integer(d[0]))

puts "#{evaluate_fine(delivered_date, expected_date)}"

