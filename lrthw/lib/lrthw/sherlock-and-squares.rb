# Solution for - https://www.hackerrank.com/challenges/sherlock-and-squares

def get_squares(first, last)
  raise ArgumentError, "Invalid input" if !(first >= 1 && first <= last && last <= 1000000000)
  squares = 0
  (first..last).each do |num|
    squares += 1 if Math.sqrt(num).modulo(1).zero?
  end
  squares
end

tests = Integer(STDIN.gets.chomp)
raise ArgumentError, "Invalid test quantity" if !tests.between?(1, 100)
squares = []
(1..tests).each do |test|
  first, last = STDIN.gets.chomp.split(" ")
  squares.push(get_squares(Integer(first), Integer(last))) 
end
squares.each do |square|
  puts "#{square}"
end

