puts "Matching Regex expression"

print "Write a number: "
number = STDIN.gets.chomp

if number =~ /\d+(\.\d{1,2})?/
  puts "This expression contains a number"
else
  puts "This expression does not contains a number"
end 

class String
  def is_number?
    true if Float(self) rescue false
  end
end

puts "Now we are going to verify if an input is_number?"
print "Write another number: "
number = STDIN.gets.chomp
puts "Is #{number} a number? #{number.is_number?}"

