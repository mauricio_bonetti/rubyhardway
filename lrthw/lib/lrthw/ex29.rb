puts "Let's play with if statements"

cats = ARGV[0].to_i
puts "Cats = #{cats}"
people = ARGV[1].to_i
puts "People = #{people}"
dogs = ARGV[2].to_i
puts "Dogs = #{dogs}"

if cats > people
  puts "More cats in the world. That is dangerous"
end

if people > cats
  puts "The world is safe, few cats ever"
end

if dogs > people
  puts "There are more dogs than people, lets celebrate!"
end

if people > dogs
  puts "There is not enough dogs, lets change the world!"
end


