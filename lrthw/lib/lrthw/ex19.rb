puts "This exercise will show how ruby handles parameters passing"

def print(value)
	value = 20
	puts value
end

print(10)
num = 30
puts "variable 'num' = #{num}"
print(num)
puts "variable 'num' = #{num}"

value = 30
puts "variable 'value' = #{value}"
print(value)
puts "variable 'value' = #{value}"
